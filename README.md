# Ansible Security Demo
This demo will showcase security automation with Ansible Tower.  My goal with this demo is to showcase a few activities of daily living in the life of a security analyst.  This will include things like firewall rule provisioning, vpn profile creation, and enriching log sources for an investigation.

## Problems Addressed
Daily activities that consume time and could be automated are often overlooked big shiny things when it comes to automation adoption.  

## Requirements
You'll need to spin up the [Ansible Security Workshop](https://github.com/ansible/workshops)

## Instructions
1. When the environment is ready, SSH into the control box or use the included Code-Server environment to open a terminal to the studentX home directory.
2. Clone this repo into that folder [ansible-security-demo](https://github.com/corumj/ansible-security-demo)
3. Update the login_info.yml file with the password for Ansible Tower
4. Run `ansible-galaxy collection install -r collections/requirements.yml`
5. Run `ansible-playbook demo_setup.yml` to create the necessary playbooks and setup permissions
6. Log in to Ansible Tower and run the Whitelist Attacker job template (this must be run at least once)
7. Log in to the CheckPoint SmartConsole tool on the Windows Workstation that is included with the environment.  You will need the IP address of the checkpoint Firewall from the Ansible Inventory, as well as the username and password from workshop exercise 1.2.
8. Follow step 2.7 in the same exercise to turn on logging for your Whitelist policy. This is why you have to run the Whitelist Attacker job template first.
9. Run the "Start DDOS Attack" job template

Note: If you haven't walked through the entire Security Automation workshop yourself, I would highly recommend it before giving this demo.

## Running the Demo
Since this demo involves switching users a few times, it's best to have extra tabs open with the user and password prepopulated (but not logged in).
We will be working with a few users for the demo, Mary, a security analyst, Kim, a firewall operator, and Carter, an intrusion detection system operator.  Their usernames are in the table below, their passwords match the password RHPDS generates randomly.

| Name   | Username    |
| ------ | ----------- |
| Mary   | analyst     |
| Kim    | opsfirewall |
| Carter | opsids      |

### Setting up the scenario
Kim, being the crack firewall operator she is, discovers some unusual traffic getting dropped by the firewall.  Since she knows that the Security Analyst will need the logs from the Firewall, she shares a job templates she created in Ansible Tower with the analyst team so they can forward the firewall logs over to QRadar, her companies SIEM solution.

1. Logged in as Kim (opsfirewall) On the Resources Menu, select Templates, and then click on the "Send firewall logs to QRadar" job template.
2. Click on the permissions tab, and the green + button, and add TeamSIEM with execute permissions.

OK, so now Kim, being responsible, creates a ticket for the Security team, and blacklists that traffic and let's the analysts know she's shared a job template that will let them enrich their logs.
3. Run the Blacklist Attacker job template.  

Mary, a security analyst is assigned the ticket and wants to start investigating.  There are several steps involved in this, she needs the logs from the Firewall and she needs to setup QRadar to process those logs.  Once she identifies the signature of this attack she will also need to setup their Snort intrusion detection system to look for this attack across the entire network, to make sure it's not a false positive, or expected application traffic.  

1. Log in to Tower as Mary (analyst).  We can take a quick look at the templates Mary has available.  She controls the templates that can accept logs into QRadar from the firewall and IDS, and now she has access to the template that will let her setup the firewall to send those logs (thanks Kim!) and a simple compliance check playbook.
2. Instead of running these templates one at a time, Mary knows she'll need to run 4 of these templates every time she needs to investigate a similar situation.  The business restricts constantly sending the logs into their SIEM because it's very costly in terms of storage and possibly money (usually a problem with Splunk since it's charged by kb of logs).  Let's make a workflow to run all of these in the right order!  Click the green + and select Workflow Template from the dropdown.  Create a workflow called "Enrich Logs" that has two branching start points with "Send IDPS logs to QRadar" and "Send Firewall logs to QRadar", followed by their respective "accept in QRadar" templates.
3. Run the new workflow template to enrich those logs.

Now Mary has the information she needs flowing into QRadar and is able to identify the signature of this particular attack.  She doesn't have access to create new IDS rules, so she sends that information over to their Snort IDS expert to create the rule.

1. Login as Carter (opsids) 
2. Carter hates repetitive tasks so he setup some automation that will take the snort rule he created from Mary's attack signature and create the rule for him.  Run the "Add IDPS Rule" job template and fill out the survey with this rule:
`alert tcp any any -> any any (msg:"Attempted DDoS Attack"; uricontent:"/ddos_simulation"; classtype:successful-dos; sid:99000010; priority:1; rev:1;)`

Carter and Mary wait eagerly to see if snort detects that attack signature from any other origin point, to any of their servers. In this case it won't since the attack is coming from a known server.

It's likely after speakign with the application owner of the server sending the 'attack' it's normal traffic and this was a false positive, so we can whitelist this,  Mary updates the ticket and let's Kim know it's okay to whitelist that traffic.  

